﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Among_Us_Tasks
{
    public partial class ucTarea2 : UserControl
    {
        public ucTarea2()
        {
            InitializeComponent();
        }

        private void btnDescargar_Click(object sender, EventArgs e)
        {
            btnDescargar.Visible = false;
            progressBar1.Visible = true;
            lblPorcentaje.Visible = true;
            lblTiempoRestante.Visible = true;
            pbxGif.Visible = true;
            tmrDescargar.Enabled = true;
        }

        private void tmrDescargar_Tick(object sender, EventArgs e)
        {
            switch (progressBar1.Value)
            {
                case 0:
                    progressBar1.Value += 14;
                    lblTiempoRestante.Text = "Estimated Time: 11hr 25m";
                    break;
                case 14:
                    progressBar1.Value += 14;
                    lblTiempoRestante.Text = "Estimated Time: 3hr 53m";
                    break;
                case 28:
                    progressBar1.Value += 14;
                    lblTiempoRestante.Text = "Estimated Time: 2hr 6m 2s";
                    break;
                case 42:
                    progressBar1.Value += 14;
                    lblTiempoRestante.Text = "Estimated Time: 34m 48s";
                    tmrDescargar.Interval = 68;
                    break;
                case 100:
                    lblTiempoRestante.Text = "Complete";
                    pbxGif.Visible = false;
                    tmrDescargar.Enabled = false;
                    label1.ForeColor = Color.Lime;

                    frmMain.TareasCompletas[1] = true;
                    break;
                default:
                    progressBar1.Value++;

                    int MilisegundosRestantes = (100 - progressBar1.Value) * tmrDescargar.Interval;
                    lblTiempoRestante.Text = "Estimated Time: " + (MilisegundosRestantes/1000 + 1) + "s";
                    break;
            }
            lblPorcentaje.Text = progressBar1.Value + "%";
        }
    }
}
