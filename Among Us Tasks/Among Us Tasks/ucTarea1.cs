﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Among_Us_Tasks
{
    public partial class ucTarea1 : UserControl
    {
        public ucTarea1()
        {
            InitializeComponent();
        }

        private void btnLlenar_MouseDown(object sender, MouseEventArgs e)
        {
            tmrLlenar.Enabled = true;
            btnRedLED.BackColor = Color.Red;
        }

        private void btnLlenar_MouseUp(object sender, MouseEventArgs e)
        {
            tmrLlenar.Enabled = false;
            btnRedLED.BackColor = Color.FromArgb(64,0,0);
        }

        private void tmrLlenar_Tick(object sender, EventArgs e)
        {
            if (panelGasolina.Height < panelTanque.Height)
            {
                panelGasolina.Height += 4;
            }
            else
            {
                btnGreenLED.BackColor = Color.Lime;
                btnRedLED.BackColor = Color.FromArgb(64, 0, 0);
                btnLlenar.Enabled = false;
                tmrLlenar.Enabled = false;
                label1.ForeColor = Color.Lime;

                frmMain.TareasCompletas[0] = true;
            }
        }

        private void ucTarea1_Load(object sender, EventArgs e)
        {
            panelGasolina.Height = 0;
        }
    }
}
