﻿
namespace Among_Us_Tasks
{
    partial class ucTarea1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelTanque = new System.Windows.Forms.Panel();
            this.panelGasolina = new System.Windows.Forms.Panel();
            this.btnGreenLED = new System.Windows.Forms.Button();
            this.btnRedLED = new System.Windows.Forms.Button();
            this.btnLlenar = new System.Windows.Forms.Button();
            this.tmrLlenar = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelTanque.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(700, 400);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(694, 82);
            this.label1.TabIndex = 0;
            this.label1.Text = "Gasolina";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 85);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(694, 312);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::Among_Us_Tasks.Properties.Resources.Fuel_Engines;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.btnGreenLED);
            this.panel2.Controls.Add(this.btnRedLED);
            this.panel2.Controls.Add(this.btnLlenar);
            this.panel2.Location = new System.Drawing.Point(228, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(342, 306);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Red;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(23, 27);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(3);
            this.panel3.Size = new System.Drawing.Size(187, 210);
            this.panel3.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.panelTanque);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(3);
            this.panel4.Size = new System.Drawing.Size(181, 204);
            this.panel4.TabIndex = 2;
            // 
            // panelTanque
            // 
            this.panelTanque.BackColor = System.Drawing.Color.Black;
            this.panelTanque.Controls.Add(this.panelGasolina);
            this.panelTanque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTanque.Location = new System.Drawing.Point(3, 3);
            this.panelTanque.Name = "panelTanque";
            this.panelTanque.Size = new System.Drawing.Size(175, 198);
            this.panelTanque.TabIndex = 3;
            // 
            // panelGasolina
            // 
            this.panelGasolina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(163)))), ((int)(((byte)(13)))));
            this.panelGasolina.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGasolina.Location = new System.Drawing.Point(0, 111);
            this.panelGasolina.Name = "panelGasolina";
            this.panelGasolina.Size = new System.Drawing.Size(175, 87);
            this.panelGasolina.TabIndex = 0;
            // 
            // btnGreenLED
            // 
            this.btnGreenLED.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnGreenLED.Enabled = false;
            this.btnGreenLED.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnGreenLED.FlatAppearance.BorderSize = 2;
            this.btnGreenLED.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGreenLED.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGreenLED.Location = new System.Drawing.Point(299, 216);
            this.btnGreenLED.Name = "btnGreenLED";
            this.btnGreenLED.Size = new System.Drawing.Size(16, 17);
            this.btnGreenLED.TabIndex = 0;
            this.btnGreenLED.UseVisualStyleBackColor = false;
            // 
            // btnRedLED
            // 
            this.btnRedLED.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRedLED.Enabled = false;
            this.btnRedLED.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnRedLED.FlatAppearance.BorderSize = 2;
            this.btnRedLED.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRedLED.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRedLED.Location = new System.Drawing.Point(264, 216);
            this.btnRedLED.Name = "btnRedLED";
            this.btnRedLED.Size = new System.Drawing.Size(16, 17);
            this.btnRedLED.TabIndex = 0;
            this.btnRedLED.UseVisualStyleBackColor = false;
            // 
            // btnLlenar
            // 
            this.btnLlenar.BackColor = System.Drawing.Color.Silver;
            this.btnLlenar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.btnLlenar.FlatAppearance.BorderSize = 2;
            this.btnLlenar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLlenar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLlenar.Location = new System.Drawing.Point(263, 242);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(54, 53);
            this.btnLlenar.TabIndex = 0;
            this.btnLlenar.Text = "FILL";
            this.btnLlenar.UseVisualStyleBackColor = false;
            this.btnLlenar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnLlenar_MouseDown);
            this.btnLlenar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnLlenar_MouseUp);
            // 
            // tmrLlenar
            // 
            this.tmrLlenar.Tick += new System.EventHandler(this.tmrLlenar_Tick);
            // 
            // ucTarea1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.tableLayoutPanel1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "ucTarea1";
            this.Size = new System.Drawing.Size(700, 400);
            this.Load += new System.EventHandler(this.ucTarea1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panelTanque.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panelTanque;
        private System.Windows.Forms.Panel panelGasolina;
        private System.Windows.Forms.Button btnLlenar;
        private System.Windows.Forms.Timer tmrLlenar;
        private System.Windows.Forms.Button btnGreenLED;
        private System.Windows.Forms.Button btnRedLED;
    }
}
