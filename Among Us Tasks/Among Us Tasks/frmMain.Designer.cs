﻿
namespace Among_Us_Tasks
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbtnInicio = new System.Windows.Forms.RadioButton();
            this.rbtnTarea1 = new System.Windows.Forms.RadioButton();
            this.rbtnTarea2 = new System.Windows.Forms.RadioButton();
            this.rbtnTarea3 = new System.Windows.Forms.RadioButton();
            this.rbtnTarea4 = new System.Windows.Forms.RadioButton();
            this.rbtnTarea5 = new System.Windows.Forms.RadioButton();
            this.rbtnTarea6 = new System.Windows.Forms.RadioButton();
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.panelInicio = new System.Windows.Forms.Panel();
            this.ucTarea61 = new Among_Us_Tasks.ucTarea6();
            this.ucTarea51 = new Among_Us_Tasks.ucTarea5();
            this.ucTarea41 = new Among_Us_Tasks.ucTarea4();
            this.ucTarea31 = new Among_Us_Tasks.ucTarea3();
            this.ucTarea21 = new Among_Us_Tasks.ucTarea2();
            this.ucTarea11 = new Among_Us_Tasks.ucTarea1();
            this.tmrVerificarTareas = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbtnInicio, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.rbtnTarea1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.rbtnTarea2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.rbtnTarea3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.rbtnTarea4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.rbtnTarea5, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.rbtnTarea6, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panelPrincipal, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(861, 461);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Among_Us_Tasks.Properties.Resources.among_us_2073273_stb7;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // rbtnInicio
            // 
            this.rbtnInicio.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnInicio.BackColor = System.Drawing.Color.Black;
            this.rbtnInicio.Checked = true;
            this.rbtnInicio.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnInicio.FlatAppearance.BorderSize = 2;
            this.rbtnInicio.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.rbtnInicio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.rbtnInicio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.rbtnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnInicio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnInicio.Location = new System.Drawing.Point(3, 153);
            this.rbtnInicio.Name = "rbtnInicio";
            this.rbtnInicio.Size = new System.Drawing.Size(144, 34);
            this.rbtnInicio.TabIndex = 2;
            this.rbtnInicio.TabStop = true;
            this.rbtnInicio.Text = "Inicio";
            this.rbtnInicio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnInicio.UseVisualStyleBackColor = false;
            this.rbtnInicio.CheckedChanged += new System.EventHandler(this.rbtnInicio_CheckedChanged);
            // 
            // rbtnTarea1
            // 
            this.rbtnTarea1.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTarea1.BackColor = System.Drawing.Color.Black;
            this.rbtnTarea1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTarea1.FlatAppearance.BorderSize = 2;
            this.rbtnTarea1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.rbtnTarea1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.rbtnTarea1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.rbtnTarea1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTarea1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTarea1.Location = new System.Drawing.Point(3, 193);
            this.rbtnTarea1.Name = "rbtnTarea1";
            this.rbtnTarea1.Size = new System.Drawing.Size(144, 34);
            this.rbtnTarea1.TabIndex = 2;
            this.rbtnTarea1.Text = "Tarea 1";
            this.rbtnTarea1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnTarea1.UseVisualStyleBackColor = false;
            this.rbtnTarea1.CheckedChanged += new System.EventHandler(this.rbtnTarea1_CheckedChanged);
            // 
            // rbtnTarea2
            // 
            this.rbtnTarea2.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTarea2.BackColor = System.Drawing.Color.Black;
            this.rbtnTarea2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTarea2.FlatAppearance.BorderSize = 2;
            this.rbtnTarea2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.rbtnTarea2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.rbtnTarea2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.rbtnTarea2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTarea2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTarea2.Location = new System.Drawing.Point(3, 233);
            this.rbtnTarea2.Name = "rbtnTarea2";
            this.rbtnTarea2.Size = new System.Drawing.Size(144, 34);
            this.rbtnTarea2.TabIndex = 2;
            this.rbtnTarea2.Text = "Tarea 2";
            this.rbtnTarea2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnTarea2.UseVisualStyleBackColor = false;
            this.rbtnTarea2.CheckedChanged += new System.EventHandler(this.rbtnTarea2_CheckedChanged);
            // 
            // rbtnTarea3
            // 
            this.rbtnTarea3.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTarea3.BackColor = System.Drawing.Color.Black;
            this.rbtnTarea3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTarea3.FlatAppearance.BorderSize = 2;
            this.rbtnTarea3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.rbtnTarea3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.rbtnTarea3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.rbtnTarea3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTarea3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTarea3.Location = new System.Drawing.Point(3, 273);
            this.rbtnTarea3.Name = "rbtnTarea3";
            this.rbtnTarea3.Size = new System.Drawing.Size(144, 34);
            this.rbtnTarea3.TabIndex = 2;
            this.rbtnTarea3.Text = "Tarea 3";
            this.rbtnTarea3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnTarea3.UseVisualStyleBackColor = false;
            this.rbtnTarea3.CheckedChanged += new System.EventHandler(this.rbtnTarea3_CheckedChanged);
            // 
            // rbtnTarea4
            // 
            this.rbtnTarea4.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTarea4.BackColor = System.Drawing.Color.Black;
            this.rbtnTarea4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTarea4.FlatAppearance.BorderSize = 2;
            this.rbtnTarea4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.rbtnTarea4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.rbtnTarea4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.rbtnTarea4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTarea4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTarea4.Location = new System.Drawing.Point(3, 313);
            this.rbtnTarea4.Name = "rbtnTarea4";
            this.rbtnTarea4.Size = new System.Drawing.Size(144, 34);
            this.rbtnTarea4.TabIndex = 2;
            this.rbtnTarea4.Text = "Tarea 4";
            this.rbtnTarea4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnTarea4.UseVisualStyleBackColor = false;
            this.rbtnTarea4.CheckedChanged += new System.EventHandler(this.rbtnTarea4_CheckedChanged);
            // 
            // rbtnTarea5
            // 
            this.rbtnTarea5.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTarea5.BackColor = System.Drawing.Color.Black;
            this.rbtnTarea5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTarea5.FlatAppearance.BorderSize = 2;
            this.rbtnTarea5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.rbtnTarea5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.rbtnTarea5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.rbtnTarea5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTarea5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTarea5.Location = new System.Drawing.Point(3, 353);
            this.rbtnTarea5.Name = "rbtnTarea5";
            this.rbtnTarea5.Size = new System.Drawing.Size(144, 34);
            this.rbtnTarea5.TabIndex = 2;
            this.rbtnTarea5.Text = "Tarea 5";
            this.rbtnTarea5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnTarea5.UseVisualStyleBackColor = false;
            this.rbtnTarea5.CheckedChanged += new System.EventHandler(this.rbtnTarea5_CheckedChanged);
            // 
            // rbtnTarea6
            // 
            this.rbtnTarea6.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTarea6.BackColor = System.Drawing.Color.Black;
            this.rbtnTarea6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTarea6.FlatAppearance.BorderSize = 2;
            this.rbtnTarea6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.rbtnTarea6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.rbtnTarea6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.rbtnTarea6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTarea6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTarea6.Location = new System.Drawing.Point(3, 393);
            this.rbtnTarea6.Name = "rbtnTarea6";
            this.rbtnTarea6.Size = new System.Drawing.Size(144, 34);
            this.rbtnTarea6.TabIndex = 2;
            this.rbtnTarea6.Text = "Tarea 6";
            this.rbtnTarea6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnTarea6.UseVisualStyleBackColor = false;
            this.rbtnTarea6.CheckedChanged += new System.EventHandler(this.rbtnTarea6_CheckedChanged);
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.Controls.Add(this.panelInicio);
            this.panelPrincipal.Controls.Add(this.ucTarea61);
            this.panelPrincipal.Controls.Add(this.ucTarea51);
            this.panelPrincipal.Controls.Add(this.ucTarea41);
            this.panelPrincipal.Controls.Add(this.ucTarea31);
            this.panelPrincipal.Controls.Add(this.ucTarea21);
            this.panelPrincipal.Controls.Add(this.ucTarea11);
            this.panelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrincipal.Location = new System.Drawing.Point(153, 3);
            this.panelPrincipal.Name = "panelPrincipal";
            this.tableLayoutPanel1.SetRowSpan(this.panelPrincipal, 9);
            this.panelPrincipal.Size = new System.Drawing.Size(705, 455);
            this.panelPrincipal.TabIndex = 3;
            // 
            // panelInicio
            // 
            this.panelInicio.BackgroundImage = global::Among_Us_Tasks.Properties.Resources.Among_Us_menu;
            this.panelInicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelInicio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInicio.Location = new System.Drawing.Point(0, 0);
            this.panelInicio.Name = "panelInicio";
            this.panelInicio.Size = new System.Drawing.Size(705, 455);
            this.panelInicio.TabIndex = 1;
            // 
            // ucTarea61
            // 
            this.ucTarea61.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ucTarea61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea61.ForeColor = System.Drawing.Color.Black;
            this.ucTarea61.Location = new System.Drawing.Point(0, 0);
            this.ucTarea61.Name = "ucTarea61";
            this.ucTarea61.Size = new System.Drawing.Size(705, 455);
            this.ucTarea61.TabIndex = 7;
            this.ucTarea61.Visible = false;
            // 
            // ucTarea51
            // 
            this.ucTarea51.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ucTarea51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea51.ForeColor = System.Drawing.Color.Black;
            this.ucTarea51.Location = new System.Drawing.Point(0, 0);
            this.ucTarea51.Name = "ucTarea51";
            this.ucTarea51.Size = new System.Drawing.Size(705, 455);
            this.ucTarea51.TabIndex = 6;
            this.ucTarea51.Visible = false;
            // 
            // ucTarea41
            // 
            this.ucTarea41.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ucTarea41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea41.ForeColor = System.Drawing.Color.Black;
            this.ucTarea41.Location = new System.Drawing.Point(0, 0);
            this.ucTarea41.Name = "ucTarea41";
            this.ucTarea41.Size = new System.Drawing.Size(705, 455);
            this.ucTarea41.TabIndex = 5;
            this.ucTarea41.Visible = false;
            // 
            // ucTarea31
            // 
            this.ucTarea31.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ucTarea31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea31.ForeColor = System.Drawing.Color.Black;
            this.ucTarea31.Location = new System.Drawing.Point(0, 0);
            this.ucTarea31.Name = "ucTarea31";
            this.ucTarea31.Size = new System.Drawing.Size(705, 455);
            this.ucTarea31.TabIndex = 4;
            this.ucTarea31.Visible = false;
            // 
            // ucTarea21
            // 
            this.ucTarea21.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ucTarea21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea21.ForeColor = System.Drawing.Color.Black;
            this.ucTarea21.Location = new System.Drawing.Point(0, 0);
            this.ucTarea21.Name = "ucTarea21";
            this.ucTarea21.Size = new System.Drawing.Size(705, 455);
            this.ucTarea21.TabIndex = 3;
            this.ucTarea21.Visible = false;
            // 
            // ucTarea11
            // 
            this.ucTarea11.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ucTarea11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea11.ForeColor = System.Drawing.Color.Black;
            this.ucTarea11.Location = new System.Drawing.Point(0, 0);
            this.ucTarea11.Name = "ucTarea11";
            this.ucTarea11.Size = new System.Drawing.Size(705, 455);
            this.ucTarea11.TabIndex = 2;
            this.ucTarea11.Visible = false;
            // 
            // tmrVerificarTareas
            // 
            this.tmrVerificarTareas.Enabled = true;
            this.tmrVerificarTareas.Tick += new System.EventHandler(this.tmrVerificarTareas_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(861, 461);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Tareas de Among Us";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelPrincipal.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelInicio;
        private System.Windows.Forms.RadioButton rbtnInicio;
        private System.Windows.Forms.RadioButton rbtnTarea1;
        private System.Windows.Forms.RadioButton rbtnTarea2;
        private System.Windows.Forms.RadioButton rbtnTarea3;
        private System.Windows.Forms.RadioButton rbtnTarea4;
        private System.Windows.Forms.RadioButton rbtnTarea5;
        private System.Windows.Forms.RadioButton rbtnTarea6;
        private System.Windows.Forms.Panel panelPrincipal;
        private ucTarea6 ucTarea61;
        private ucTarea5 ucTarea51;
        private ucTarea4 ucTarea41;
        private ucTarea3 ucTarea31;
        private ucTarea2 ucTarea21;
        private ucTarea1 ucTarea11;
        private System.Windows.Forms.Timer tmrVerificarTareas;
    }
}

